import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import App from '../App';
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';




function Profile() {

    const email_key = 'email';

    const [user, setUser] = useState(read_cookie(email_key));

    function logout() {
        delete_cookie(email_key);
        delete_cookie("token");
	}

    return (
        <div className="profile">
            <div className="page-container">
                <div className="myHeader">
                    <div className="d-grid gap-2 mt-3">
                        <NavLink to="/">
                            <button className="btn">
                                Home
                            </button>
                        </NavLink>
                        <NavLink to="/profile">
                            <button className="btn">
                                Profile
                            </button>
                        </NavLink>
                        <NavLink to="/signin">
                            <button type="submit" onClick={() => logout()} className="btn logoutbtn">
                                Logout
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>

            <div class="page-container">
                <div className="Simple-form">
                    <div class="mycontent">
                        <h1 class="font-weight-light">Profile</h1>
                        <p>
                            You are logged in as: {user}
                        </p>

                    </div>
                </div>

            </div>
        </div>
    );
}

export default Profile;