import React from "react";
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';
import { useState } from 'react';
import { Routes, Route, useNavigate } from 'react-router-dom';

function SignIn(props) {

    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [pw, setPw] = useState('');


    // I should post the values to a backend database
    // but for the demo I'm going to check if the email is demo@hotmail.com
    // and the password is secretpassword

    const handleSubmit = event => {
        console.log('handleSubmit ran');
        event.preventDefault();

        if (name == "demo@hotmail.com" && pw == "secretpassword") {
            bake_cookie('email', 'demo@hotmail.com');
            bake_cookie('token', '12345');
            navigate('/');
        } else {
            console.log("wrong :(");
        }
    }


    return (
        <div className="Auth-form-container">
            <form onSubmit={handleSubmit} className="Auth-form">
                <div className="Auth-form-content">
                    <h3 className="Auth-form-title">Sign in</h3>
                    <div className="form-group mt-3">
                        <label>Email address</label>
                        <input
                            type="email"
                            name="email"
                            className="form-control mt-1"
                            placeholder="Enter email"
                            onChange={event => setName(event.target.value)}
                        />
                    </div>
                    <div className="form-group mt-3">
                        <label>Password</label>
                        <input
                            type="password"
                            name="password"
                            className="form-control mt-1"
                            placeholder="Enter password"
                            onChange={event => setPw(event.target.value)}
                        />
                    </div>
                    <div className="d-grid gap-2 mt-3">
                        <button type="submit"  className="btn btn-primary">
                            Submit
                        </button>
                    </div>
                    <p className="forgot-password text-right mt-2">
                        Forgot <a href="#">password?</a>
                    </p>
                </div>
            </form>
        </div>
    );
}

export default SignIn;