import React from "react";
import { NavLink } from "react-router-dom";
import App from '../App';


function Home() {
    return (
        <div className="home">
            <div className="page-container">
                <div className="myHeader">
                    <div className="d-grid gap-2 mt-3">
                        <NavLink  to="/">
                            <button  className="btn">
                                Home
                            </button>
                        </NavLink>
                        <NavLink to="/profile">
                            <button className="btn">
                                Profile
                            </button>
                        </NavLink>
                        <NavLink to="/signin">
                            <button type="submit" className="btn logoutbtn">
                            Logout
                            </button>
                        </NavLink>
                    </div>
                </div>
            </div>

            <div class="page-container">
                <div className="Simple-form">
                    <div class="mycontent">
                        <h1 class="font-weight-light">Home</h1>
                        <p>
                            In this example we use Redux to deposit or withdraw money from the bank. The code for this can be seen in the map "state"
                        </p>
                        
                    </div>
                    <App />
                </div>
                
            </div>
        </div>
  );
}

export default Home;