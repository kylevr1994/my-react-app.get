import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "./state/index";

function App() {



    const account = useSelector((state) => state.account);
    const dispatch = useDispatch();

    const { depositMoney, withdrawMoney } = bindActionCreators(actionCreators, dispatch);

    //console.log(depositMoney);

  return (
      <div className="App">
          <h1>The bank: {account},-</h1>
          <button className="btn-bank" onClick={() => depositMoney(500)}>deposit</button>
          <button className="btn-bank" onClick={() => withdrawMoney(500)}>withdraw</button>
    </div>
  );
}

export default App;
