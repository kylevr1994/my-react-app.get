import { Outlet, Navigate } from "react-router-dom";
import SignIn from "./components/SignIn";
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';


const useAuth = () => {

    // Normally i should check if the token is correct in the backend but
    // for this example I'm just gonna check if token is equals to 12345
    let token = 'token';

    if ((read_cookie(token) == "12345")) {
        const user = { loggedIn: true }
        return user && user.loggedIn;
    } else {
        const user = { loggedIn: false }
        return user && user.loggedIn;
    }
        
}

const ProtectedRoutes = () => {
    const isAuth = useAuth();
    return isAuth ? <Outlet /> : <Navigate to="/signin" />;
}

export default ProtectedRoutes;