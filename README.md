
You can login to with the following credentials:<br>
**email:** demo@hotmail.com <br>
**password:** secretpassword

***

**Note:** login is simple and checks if the values are the above credentials with a simple if(). I didn't create a backend for this. If you want to see this I can show it on another project or I can give you access to it. 

Redux is used and can be seen on the homepage. On the profile page you can see with what email adress you are logged in. The app is adaptive and can be shown on different screen sizes.

***

**Run application:** Npm run start
